package com.lxl.edu.info.manager.factory;


import com.lxl.edu.info.manager.dao.BaseTeacherDao;
import com.lxl.edu.info.manager.dao.TeacherDao;

public class TeacherFactory {
    public static BaseTeacherDao getTeacherDao(){
        return new TeacherDao();
    }
}
