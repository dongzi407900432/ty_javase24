package com.lxl.edu.info.manager.dao;

import com.lxl.edu.info.manager.domain.Teacher;

import java.util.ArrayList;

public interface BaseTeacherDao {



    public abstract boolean addTeacher(Teacher t);

    public abstract Teacher[] findAllTeacher();

    public abstract void deleteTeacherById(String id) ;

    public abstract int getIndex(String id);

    public abstract void updateTeacher(String id, Teacher newTeacher) ;
}