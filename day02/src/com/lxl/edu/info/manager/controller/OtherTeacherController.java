package com.lxl.edu.info.manager.controller;


import com.lxl.edu.info.manager.domain.Teacher;

import java.util.Scanner;

public class OtherTeacherController extends BaseTeacherController{
    private Scanner sc = new Scanner(System.in);

    // 录入老师信息, 封装为老师对象
    public Teacher inputTeacherInfo(String id){
        System.out.println("请输入老师姓名:");
        String name = sc.next();
        System.out.println("请输入老师年龄:");
        String age = sc.next();
        System.out.println("请输入老师生日:");
        String birthday = sc.next();

        Teacher t = new Teacher(id,name,age,birthday);


        return t;
    }


}
