package com.lxl.edu.info.manager.dao;

import com.lxl.edu.info.manager.domain.Student;
import com.lxl.edu.info.manager.domain.Teacher;

public class TeacherDao implements BaseTeacherDao {

    private static Teacher[] teachers = new Teacher[5];
    static {
        Teacher stu = new Teacher("001","yiyi","20","1990-11-11");
        Teacher stu1 = new Teacher("002","en","20","1991-11-11");
        Teacher stu2 = new Teacher("003","nen","20","1992-11-11");
        teachers[0]=stu;
        teachers[1]=stu1;
        teachers[2]=stu2;

    }

    public boolean addTeacher(Teacher t) {
        int index = -1;
        for (int i = 0; i < teachers.length; i++) {
            Teacher teacher = teachers[i];
            if(teacher == null){
                index = i;
                break;
            }
        }

        if(index == -1){
            return false;
        }else{
            teachers[index] = t;
            return true;
        }

    }

    public Teacher[] findAllTeacher() {
        return teachers;
    }

    public void deleteTeacherById(String id) {
        // 1. 查询id在数组中的索引位置
        int index = getIndex(id);
        // 2. 将该索引位置的元素, 使用null进行替换
        teachers[index] = null;
    }





    public int getIndex(String id){
        int index = -1;
        for (int i = 0; i < teachers.length; i++) {
            Teacher t = teachers[i];
            if(t != null && t.getId().equals(id)){
                index = i;
                break;
            }
        }

        return index;
    }


    public void updateTeacher(String id, Teacher newTeacher) {
        int index = getIndex(id);
        teachers[index] = newTeacher;
    }
}