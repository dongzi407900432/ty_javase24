package com.lxl.edu.info.manager.dao;

import com.lxl.edu.info.manager.domain.Student;

import java.util.ArrayList;

public class OtherStudentDao implements BaseStudentDao {
    // 创建学生对象数组
    //private static Student[] stus = new Student[5];
    private static ArrayList<Student> stus = new ArrayList<>();
    static {
        Student stu = new Student("001","yiyi","20","1990-11-11");
        Student stu1 = new Student("002","en","20","1991-11-11");
        Student stu2 = new Student("003","nen","20","1992-11-11");
        stus.add(stu);
        stus.add(stu1);
        stus.add(stu2);


    }


    // 添加学生方法
    public boolean addStudent(Student stu) {

        stus.add(stu);
        return true;
    }
    // 查看学生方法
    public Student[] findAllStudent() {
        Student[] stuss = new Student[stus.size()];
        for (int i = 0; i < stuss.length; i++) {
            stuss[i] = stus.get(i);
        }

        return stuss;
    }

    public void deleteStudentById(String delId) {
        // 1. 查找id在容器中所在的索引位置
        int index = getIndex(delId);
        // 2. 将该索引位置,使用null元素进行覆盖
        stus.remove(index);
    }

    public int getIndex(String id){
        int index = -1;
        for (int i = 0; i < stus.size(); i++) {
            Student stu = stus.get(i);
            if(stu != null && stu.getId().equals(id)){
                index = i;
                break;
            }
        }
        return index;
    }

    public void updateStudent(String updateId, Student newStu) {
        // 1. 查找updateId, 在容器中的索引位置
        int index = getIndex(updateId);
        // 2. 将该索引位置, 使用新的学生对象替换
        stus.set(index,newStu);
    }
}
