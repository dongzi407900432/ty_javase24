package com.lxl.edu.info.manager.dao;

import com.lxl.edu.info.manager.domain.Student;
import com.lxl.edu.info.manager.domain.Teacher;

import java.util.ArrayList;

public class OtherTeacherDao implements BaseTeacherDao{

//    private static Teacher[] teachers = new Teacher[5];

    private static ArrayList<Teacher> teachers = new ArrayList<>();
    static {
        Teacher stu = new Teacher("001","yiyi","20","1990-11-11");
        Teacher stu1 = new Teacher("002","en","20","1991-11-11");
        Teacher stu2 = new Teacher("003","nen","20","1992-11-11");
        teachers.add(stu);
        teachers.add(stu1);
        teachers.add(stu2);
    }

    public boolean addTeacher(Teacher t) {
        teachers.add(t);
        return true;

    }

    public Teacher[] findAllTeacher() {
       Teacher[] th =  new Teacher[teachers.size()];
        for (int i = 0; i < th.length; i++) {
            th[i]=teachers.get(i);

        }
        return th;
    }

    public void deleteTeacherById(String id) {
        // 1. 查询id在数组中的索引位置
        int index = getIndex(id);
        // 2. 将该索引位置的元素, 使用null进行替换
       teachers.remove(index);
    }



    public int getIndex(String id){
        int index = -1;
        for (int i = 0; i < teachers.size(); i++) {
            Teacher t = teachers.get(i);
            if(t != null && t.getId().equals(id)){
                index = i;
                break;
            }
        }

        return index;
    }



    public void updateTeacher(String id, Teacher newTeacher) {
        int index = getIndex(id);
        teachers.set(index,newTeacher);
    }
}