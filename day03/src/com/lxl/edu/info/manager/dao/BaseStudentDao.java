package com.lxl.edu.info.manager.dao;

import com.lxl.edu.info.manager.domain.Student;

public  interface BaseStudentDao {



    // 添加学生方法
    public abstract boolean addStudent(Student stu) ;
    // 查看学生方法
    public abstract Student[] findAllStudent() ;

    public abstract void deleteStudentById(String delId) ;

    public abstract int getIndex(String id);

    public abstract void updateStudent(String updateId, Student newStu);
}
