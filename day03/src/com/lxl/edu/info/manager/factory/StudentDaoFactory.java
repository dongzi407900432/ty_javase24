package com.lxl.edu.info.manager.factory;

import com.lxl.edu.info.manager.dao.OtherStudentDao;

public class StudentDaoFactory {
    public static OtherStudentDao getStudentDao(){
        return new OtherStudentDao();
    }
}
