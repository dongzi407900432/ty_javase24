package com.lxl.edu.info.manager.factory;

        import com.lxl.edu.info.manager.dao.OtherStudentDao;
        import com.lxl.edu.info.manager.dao.OtherTeacherDao;

public class TeacherDaoFactory {
    public static OtherTeacherDao getTeacherDao(){
        return new OtherTeacherDao();
    }
}
