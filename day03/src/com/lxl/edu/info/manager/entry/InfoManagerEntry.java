package com.lxl.edu.info.manager.entry;


import com.lxl.edu.info.manager.controller.OtherStudentController;
import com.lxl.edu.info.manager.controller.OtherTeacherController;


import java.util.Scanner;

public class InfoManagerEntry {
    public static void main(String[] args) {
        //搭建主菜单
        Scanner sc = new Scanner(System.in);

      while (true){
            System.out.println("--------欢迎来到黑马信息管理系统--------");
            System.out.println("请输入您的选择: 1.学生管理  2.老师管理  3.退出");
            String choice= sc.next();
            switch (choice){
                case "1":
                    //System.out.println("1.学生管理");
                    //开启学生管理系统
                    OtherStudentController studentController = new OtherStudentController();


                    studentController.start();
                    break;
                case "2":
                   //System.out.println("2.老师管理");
                   OtherTeacherController teacherController = new OtherTeacherController();
                    teacherController.start();
                    break;
                case "3":
                    System.out.println("3.退出黑马信息管理系统");
                    System.exit(0);
                    //break;
                default:
                    System.out.println("您的输入有误，请重新输入：");
                    break;
            }
        }

    }
}
